<?php

return [
    'category' => [
        'appTitle'           => 'Shrym CMS - Category Manager.',
        'cantDeleteRootNode' => 'You can\'t delete the Root node.',
        'nodeHasChild'       => 'This node has children node, please delete children node first.',
        'cantMoveYourSelf'   => 'Can\'t move to your self.'
    ],
];