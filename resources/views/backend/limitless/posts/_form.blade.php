<div class="panel-body">
    <div class="form-group">
        <label>Thumbnail:</label>
        {!! Form::file('thumbnail', '',['class' => 'form-control']) !!}
        @if ($errors->has('thumbnail'))
            {!! Form::fieldError('thumbnail') !!}
        @endif
    </div>

    <div class="form-group">
        <label>Name:</label>
        {!! Form::text('name', null,['class' => 'form-control']) !!}
        @if ($errors->has('name'))
            {!! Form::fieldError('name') !!}
        @endif
    </div>

    <div class="form-group">
        <label>Slug:</label>
        {!! Form::text('slug', null,['class' => 'form-control']) !!}
        <span class="help-block">Default slug is: <span id="default-slug" class="text-blue-700"></span></span>
        @if ($errors->has('slug'))
            {!! Form::fieldError('slug') !!}
        @endif
    </div>

    <div class="form-group">
        <label>Category:</label>
        {!! Form::select('category', $categories,null, ['class' => 'select']) !!}
        @if ($errors->has('category'))
            {!! Form::fieldError('category') !!}
        @endif
    </div>

    <div class="form-group">
        <label>Description:</label>
        @if ($errors->has('description'))
            {!! Form::fieldError('description') !!}
        @endif
        {!! Form::textarea('description',null,['class' => 'form-control summernote']); !!}
    </div>

    <div class="form-group">
        <label>Content:</label>
        @if ($errors->has('content'))
            {!! Form::fieldError('content') !!}
        @endif
        {!! Form::textarea('content',null,['class' => 'form-control summernote-height']); !!}
    </div>

    <div class="form-group">
        <label class="display-block">Public:</label>

        <label class="radio-inline">
            {!! Form::radio('public', '1', true,['class'=>'styled']) !!} Active
        </label>

        <label class="radio-inline">
            {!! Form::radio('public', '0', false, ['class'=>'styled']) !!} Inactive
        </label>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-primary">Submit <i
                    class="icon-arrow-right14 position-right"></i></button>
    </div>
</div>