@extends('backend.limitless.app')

{{--Begin Config--}}
@section('app.title','Shrym CMS - Edit Post.')
{!! Form::component('fieldError', 'backend.limitless.partials.components.input-validate-error', ['name']) !!}
{{--End Config--}}

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($post, ['route' => ['admin.posts.update',$post->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit post</h5>
                </div>
                @include('backend.limitless.posts._form')
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/editor_summernote.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend/partials/helpers.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend/posts/page.js') }}"></script>
@endpush