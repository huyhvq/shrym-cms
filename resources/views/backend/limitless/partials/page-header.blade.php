<div class="page-header-content">
    <div class="page-title">
        <h4>
            @section('page-title')
                <i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard
            @show
        </h4>
    </div>
</div>