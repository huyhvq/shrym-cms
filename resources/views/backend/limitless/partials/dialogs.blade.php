@if ($errors->has('swalError'))
    <script type="text/javascript">
        swal({
            title: "Oops...",
            text: "{!! $errors->first('swalError') ?? 'Something went wrong!' !!}",
            confirmButtonColor: "#EF5350",
            type: "error"
        });
    </script>
@elseif($errors->has('swalSuccess'))
    <script type="text/javascript">
        swal({
            title: "Success !!",
            text: "{{ $errors->first('swalSuccess') ?? 'Your action has be done.' }}",
            confirmButtonColor: "#66BB6A",
            type: "success"
        });
    </script>
@endif