{{--*/ $routeName = request()->route()->getName(); /*--}}
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li><a href="{{action('Backend\DashboardController@index')}}"><i class="icon-home4"></i>
                    <span>Dashboard</span></a></li>
            <li>
                <a href="#"><i class="icon-stack2"></i> <span>CMS</span></a>
                <ul>
                    <li @if($routeName == 'admin.dashboard.index') class="active" @endif><a href="#">Users</a>
                    </li>
                    <li @if($routeName == 'admin.categories.index' || $routeName == 'admin.categories.edit') class="active" @endif>
                        <a href="{!! route('admin.categories.index') !!}">Categories</a></li>
                    <li id="posts-nav"><a href="boxed_default.html">Posts</a></li>
                </ul>
            </li>
            <!-- /page kits -->
        </ul>
    </div>
</div>