@extends('backend.limitless.app')

{{--Begin Config--}}
@section('app.title','Shrym CMS - Admin Dashboard.')
{!! Form::component('fieldError', 'backend.limitless.partials.components.input-validate-error', ['name']) !!}
{{--End Config--}}

@section('content')
    <div class="row">
        <div class="col-md-4">
            <!— Basic layout—>
            {!! Form::model($user=null) !!}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Create new categories</h5>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label>Name:</label>
                        {!! Form::text('name', '',['class' => 'form-control', 'placeholder' => 'Category Name']) !!}
                        @if ($errors->has('name'))
                            {!! Form::fieldError('name') !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Slug:</label>
                        {!! Form::text('slug', '',['class' => 'form-control', 'placeholder' => 'Category Slug']) !!}
                        <span class="help-block">Default slug is: <span id="default-slug" class="text-blue-700"></span></span>
                        @if ($errors->has('slug'))
                            {!! Form::fieldError('slug') !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Parrent Category:</label>
                        <select name="parent_id" class="select">
                            @foreach($categories as $cateKey => $category)
                                <option value="{{ $cateKey }}">{{ $category }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            {!! Form::fieldError('parent_id') !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="display-block">Public:</label>

                        <label class="radio-inline">
                            {!! Form::radio('public', '1', true,['class'=>'styled']) !!} Active
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('public', '0', false, ['class'=>'styled']) !!} Not active
                        </label>
                    </div>

                    <div class="form-group">
                        <label>Description:</label>
                        {!! Form::textarea('description',null,['rows' => 5, 'cols' => 5, 'class' => 'form-control', 'placeholder' => 'Category description']); !!}
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Create <i
                                    class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <!— /basic layout —>

        </div>
        <div class="col-md-8">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Categories</h5>
                </div>

                <div class="panel-body">
                    List all categories in system.
                </div>

                <table class="table datatable-responsive-column-controlled">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Parrent</th>
                        <th>Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tableCategoriesData as $category)
                        <tr>
                            <td></td>
                            <td>
                                <a href="{!! route('admin.categories.edit',$category->id) !!}">
                                    {!! str_repeat('—', $category->depth) . $category->name !!}
                                </a>
                            </td>
                            <td>{{ $category->slug }}</td>
                            <td>
                                @if(is_null($category->parent))
                                    —
                                @else
                                    {{ $category->parent->name }}
                                @endif
                            </td>
                            <td>
                                @if($category->public)
                                    <span class="label label-flat border-success text-success-600">Actived</span>
                                @else
                                    <span class="label label-flat border-danger text-danger-600">In Active</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li>
                                        <a href="{!! route('admin.categories.edit',$category->id) !!}">
                                            <i class="icon-pencil"></i>
                                        </a>
                                    </li>
                                    <li>
                                        {!! Form::open(array('action' => array('Backend\CategoryController@destroy', $category->id),'method'=>'DELETE',)) !!}
                                        <a href="#" class="remove-btn">
                                            <i class="icon-cross2"></i>
                                        </a>
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend/partials/datatable.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend/partials/helpers.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend/categories/index.js') }}"></script>
@endpush