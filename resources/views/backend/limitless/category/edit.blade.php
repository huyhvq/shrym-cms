@extends('backend.limitless.app')

{{--Begin Config--}}
@section('app.title','Shrym CMS - Admin Dashboard.')
{!! Form::component('fieldError', 'backend.limitless.partials.components.input-validate-error', ['name']) !!}
{{--End Config--}}
@section('page-title')
    <i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Categories</span> - Create
@endsection
@section('breadcrumb')
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{!! route('admin.dashboard.index') !!}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{!! route('admin.categories.index') !!}">Categories</a></li>
            <li class="active">{{ $category->name }}</li>
        </ul>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($category,['route' => ['admin.categories.update', $category->id], 'method' => 'PUT']) !!}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit category</h5>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label>Name:</label>
                        {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Category Name']) !!}
                        @if ($errors->has('name'))
                            {!! Form::fieldError('name') !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Slug:</label>
                        {!! Form::text('slug', null,['class' => 'form-control', 'placeholder' => 'Category Slug']) !!}
                        <span class="help-block">Default slug is: <span id="default-slug" class="text-blue-700"></span></span>
                        @if ($errors->has('slug'))
                            {!! Form::fieldError('slug') !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Parrent Category:</label>
                        {!! Form::select('parent_id', $categories,null, ['class' => 'select']) !!}
                        @if ($errors->has('parent_id'))
                            {!! Form::fieldError('parent_id') !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="display-block">Public:</label>

                        <label class="radio-inline">
                            {!! Form::radio('public', '1', true,['class'=>'styled']) !!} Active
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('public', '0', false, ['class'=>'styled']) !!} Not active
                        </label>
                    </div>

                    <div class="form-group">
                        <label>Description:</label>
                        {!! Form::textarea('description',null,['rows' => 5, 'cols' => 5, 'class' => 'form-control', 'placeholder' => 'Category description']); !!}
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Create <i
                                    class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>
@endpush