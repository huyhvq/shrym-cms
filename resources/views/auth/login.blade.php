@extends('auth.auth')
@section('panel-body')
<!-- Tabbed form -->
<div class="tabbable panel login-form width-400">
    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a href="{{url('/login')}}"><h6><i class="icon-checkmark3 position-left"></i> Already a user?
                </h6></a></li>
        <li><a href="{{url('/register')}}"><h6><i class="icon-plus3 position-left"></i> Create an account</h6></a></li>
    </ul>

    <div class="tab-content panel-body">
        <div class="fade in" id="basic-tab1">
            <form method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <div class="text-center">
                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                    <h5 class="content-group">Login to your account
                        <small class="display-block">Your credentials</small>
                    </h5>
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="text" class="form-control" placeholder="Username" name="username"
                           value="{{ old('username') }}">
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if ($errors->has('username'))
                        <label id="username-error" class="validation-error-label"
                               for="username">{{$errors->first('username')}}</label>
                    @endif
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                    @if ($errors->has('password'))
                        <label id="password-error" class="validation-error-label"
                               for="username">{{$errors->first('password')}}</label>
                    @endif
                </div>

                <div class="form-group login-options">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" class="styled" name="remember">
                                Remember
                            </label>
                        </div>

                        <div class="col-sm-6 text-right">
                            <a href="{{ url('/password/reset') }}">Forgot password?</a>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block">Login <i
                                class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>

            <span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a
                        href="login_tabbed.html#">Terms &amp; Conditions</a> and <a href="login_tabbed.html#">Cookie
                    Policy</a></span>
        </div>

    </div>
</div>
<!-- /tabbed form -->
@endsection