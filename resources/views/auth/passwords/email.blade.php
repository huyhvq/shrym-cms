@extends('auth.auth')
@section('panel-body')
        <!-- Password recovery -->
<form method="POST" action="{{ url('/password/email') }}">
    {!! csrf_field() !!}
    <div class="panel panel-body login-form">
        <div class="text-center">
            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
            <h5 class="content-group">Password recovery
                <small class="display-block">We'll send you instructions in email</small>
            </h5>

            @if (session('status'))
                <div class="alert alert-styled-left alert-success alert-arrow-left alpha-teal alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                class="sr-only">Close</span></button>
                    {{ session('status') }}
                </div>
            @endif

        </div>

        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Your email" name="email">
            <div class="form-control-feedback">
                <i class="icon-mail5 text-muted"></i>
            </div>

            @if ($errors->has('email'))
                <label id="email-error" class="validation-error-label"
                       for="email">{{$errors->first('email')}}</label>
            @endif
        </div>


        <button type="submit" class="btn bg-blue btn-block">Reset password <i
                    class="icon-arrow-right14 position-right"></i></button>

        <span class="help-block text-center mt-10"><a href="{{ url('/login') }}"><i class="icon-undo2"></i> Return to login page.</a></span>
    </div>
</form>
<!-- /password recovery -->
@endsection