@extends('auth.auth')
@section('panel-body')
        <!-- Password recovery -->
<form method="POST" action="{{ url('/password/reset') }}">
    {!! csrf_field() !!}

    <input type="hidden" name="token" value="{{ $token }}">
    <div class="panel panel-body login-form">
        <div class="text-center">
            <div class="icon-object border-success-600 text-success-600"><i class="icon-wrench"></i></div>
            <h5 class="content-group">Update your password</h5>
        </div>

        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Your email" name="email"
                   value="{{ $email or old('email') }}">
            <div class="form-control-feedback">
                <i class="icon-mail5 text-muted"></i>
            </div>

            @if ($errors->has('email'))
                <label id="email-error" class="validation-error-label"
                       for="email">{{$errors->first('email')}}</label>
            @endif
        </div>

        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Create password" name="password">
            <div class="form-control-feedback">
                <i class="icon-user-lock text-muted"></i>
            </div>
            @if ($errors->has('password'))
                <label id="password-error" class="validation-error-label"
                       for="password">{{$errors->first('password')}}</label>
            @endif
        </div>

        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Confirm Password"
                   name="password_confirmation">
            <div class="form-control-feedback">
                <i class="icon-user-check text-muted"></i>
            </div>
        </div>

        <button type="submit" class="btn bg-blue btn-block">Reset password <i
                    class="icon-arrow-right14 position-right"></i></button>
    </div>
</form>
<!-- /password recovery -->
@endsection