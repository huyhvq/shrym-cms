@extends('auth.auth')
@section('panel-body')
    <div class="tabbable panel login-form width-400">
        <ul class="nav nav-tabs nav-justified">
            <li><a href="{{url('/login')}}"><h6><i class="icon-checkmark3 position-left"></i> Already a user?</h6></a>
            </li>
            <li class="active"><a href="{{url('/register')}}"><h6><i class="icon-plus3 position-left"></i> Create an
                        account</h6></a></li>
        </ul>

        <div class="tab-content panel-body">
            <div class="fade in" id="basic-tab2">
                <form method="POST" action="{{ url('/register') }}">
                    {!! csrf_field() !!}
                    <div class="text-center">
                        <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                        <h5 class="content-group">Create new account
                            <small class="display-block">All fields are required</small>
                        </h5>
                    </div>

                    <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control" placeholder="Your Name" name="name"
                               value="{{ old('name') }}">
                        <div class="form-control-feedback">
                            <i class="icon-user-tie text-muted"></i>
                        </div>
                        @if ($errors->has('name'))
                            <label id="name-error" class="validation-error-label"
                                   for="name">{{$errors->first('name')}}</label>
                        @endif
                    </div>

                    <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control" placeholder="Your username" name="username"
                               value="{{ old('username') }}">
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                        @if ($errors->has('username'))
                            <label id="username-error" class="validation-error-label"
                                   for="username">{{$errors->first('username')}}</label>
                        @endif
                    </div>

                    <div class="form-group has-feedback has-feedback-left">
                        <input type="password" class="form-control" placeholder="Create password" name="password">
                        <div class="form-control-feedback">
                            <i class="icon-user-lock text-muted"></i>
                        </div>
                        @if ($errors->has('password'))
                            <label id="password-error" class="validation-error-label"
                                   for="password">{{$errors->first('password')}}</label>
                        @endif
                    </div>

                    <div class="form-group has-feedback has-feedback-left">
                        <input type="password" class="form-control" placeholder="Confirm Password"
                               name="password_confirmation">
                        <div class="form-control-feedback">
                            <i class="icon-user-check text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control" placeholder="Your email" name="email" value="{{ old('email') }}">
                        <div class="form-control-feedback">
                            <i class="icon-mention text-muted"></i>
                        </div>
                        @if ($errors->has('email'))
                            <label id="email-error" class="validation-error-label"
                                   for="email">{{$errors->first('email')}}</label>
                        @endif
                    </div>

                    <div class="content-divider text-muted form-group"><span>Additions</span></div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" name="acceptTerms">
                                Accept <a href="#">terms of service</a>
                            </label>
                        </div>
                        @if ($errors->has('acceptTerms'))
                            <label id="email-error" class="validation-error-label"
                                   for="email">{{$errors->first('acceptTerms')}}</label>
                        @endif
                    </div>

                    <button type="submit" class="btn bg-indigo-400 btn-block">Register <i
                                class="icon-circle-right2 position-right"></i></button>
                </form>
            </div>
        </div>
    </div>
@endsection