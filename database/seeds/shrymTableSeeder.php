<?php
use Illuminate\Database\Seeder;

class ShrymTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([

            [
                'id'             => 1,
                'name'           => 'Administrator',
                'username'       => 'admin',
                'email'          => 'admin@shrym.com',
                'password'       => '$2y$10$3Wly3LDiIpTZlyKtcKo2oeRoK5FfyrdirnW1chruYuragX9EIkUn6',
                'remember_token' => NULL,
                'created_at'     => '2016-01-26 09:28:38',
                'updated_at'     => '2016-01-26 09:28:38',
            ]

        ]);
        DB::table('permissions')->insert([

            [
                'id'         => 1,
                'name'       => 'delete_posts',
                'label'      => 'Delete Posts',
                'created_at' => '2016-01-26 16:42:22',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 2,
                'name'       => 'delete_private_posts',
                'label'      => 'Delete Private Posts',
                'created_at' => '2016-01-26 16:42:26',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 3,
                'name'       => 'delete_published_posts',
                'label'      => 'Delete Published Posts',
                'created_at' => '2016-01-26 16:42:29',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 4,
                'name'       => 'edit_others_posts',
                'label'      => 'Edit Others Posts',
                'created_at' => '2016-01-26 16:42:33',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 5,
                'name'       => 'edit_posts',
                'label'      => 'Edit Posts',
                'created_at' => '2016-01-26 16:42:36',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 6,
                'name'       => 'edit_private_posts',
                'label'      => 'Edit Private Posts',
                'created_at' => '2016-01-26 16:42:40',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 7,
                'name'       => 'edit_published_posts',
                'label'      => 'Edit Published Posts',
                'created_at' => '2016-01-26 16:42:44',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 8,
                'name'       => 'export',
                'label'      => 'Export',
                'created_at' => '2016-01-26 09:41:58',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 9,
                'name'       => 'import',
                'label'      => 'Import',
                'created_at' => '2016-01-26 09:41:58',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 10,
                'name'       => 'list_users',
                'label'      => 'List Users',
                'created_at' => '2016-01-26 16:42:48',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 11,
                'name'       => 'manage_categories',
                'label'      => 'Manage Categories',
                'created_at' => '2016-01-26 16:42:51',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 12,
                'name'       => 'manage_links',
                'label'      => 'Manage Links',
                'created_at' => '2016-01-26 16:42:54',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 13,
                'name'       => 'manage_options',
                'label'      => 'Manage Options',
                'created_at' => '2016-01-26 16:42:58',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 14,
                'name'       => 'promote_users',
                'label'      => 'Promote Users',
                'created_at' => '2016-01-26 16:43:01',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 15,
                'name'       => 'publish_posts',
                'label'      => 'Publish Posts',
                'created_at' => '2016-01-26 16:43:07',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 16,
                'name'       => 'read_private_posts',
                'label'      => 'Read Private Posts',
                'created_at' => '2016-01-26 16:43:11',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 17,
                'name'       => 'read',
                'label'      => 'Read',
                'created_at' => '2016-01-26 09:41:58',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 18,
                'name'       => 'remove_users',
                'label'      => 'Remove Users',
                'created_at' => '2016-01-26 16:43:14',
                'updated_at' => '2016-01-26 09:41:58',
            ],

            [
                'id'         => 19,
                'name'       => 'upload_files',
                'label'      => 'Upload Files',
                'created_at' => '2016-01-26 16:43:18',
                'updated_at' => '2016-01-26 09:41:58',
            ],

        ]);
        DB::table('roles')->insert([

            [
                'id'         => 1,
                'name'       => 'super_admin',
                'label'      => 'Super Administrator',
                'created_at' => '2016-01-26 09:44:57',
                'updated_at' => '2016-01-26 09:44:57',
            ]

        ]);
        DB::table('permission_role')->insert([

            [
                'permission_id' => 1,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 2,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 3,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 4,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 5,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 6,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 7,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 8,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 9,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 10,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 11,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 12,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 13,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 14,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 15,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 16,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 17,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 18,
                'role_id'       => 1,
            ],

            [
                'permission_id' => 19,
                'role_id'       => 1,
            ],

        ]);
        DB::table('role_user')->insert([

            [
                'role_id' => 1,
                'user_id' => 1,
            ]

        ]);

        DB::table('categories')->insert([
            [
                'lft'         => 1,
                'rgt'         => 2,
                'depth'       => 0,
                'name'        => 'Root',
                'slug'        => 'root',
                'public'      => 1,
                'description' => 'Root categories'
            ]
        ]);
    }
}