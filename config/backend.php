<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Backend Path
    |--------------------------------------------------------------------------
    |
    | This option controls the default backend path of Shrym CMS.
    |
    */

    'path' => 'admin',

];
