$(function () {
    // Basic responsive configuration
    $('.datatable-responsive').DataTable();

    // Column controlled child rows
    $('.datatable-responsive-column-controlled').DataTable({
        responsive: {
            details: {
                type: 'column'
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0
            },
            {
                width: "100px",
                targets: [4]
            },
            {
                orderable: false,
                targets: [0, 1, 2, 3, 4]
            }
        ],
        paging: false,
    });

    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

});

$(document).ready(function () {
    var nameInput = $('input[name="name"]');
    var slugInput = $('input[name="slug"]');
    var defaultSlug = $('#default-slug');
    var deleteBtn = $('.remove-btn');

    nameInput.on('input', function () {
        slugInput.val() == "" ? slug = nameInput.val() : slug = slugInput.val();
        defaultSlug.text(convertToSlug(slug.trim()));
    });

    slugInput.on('input', function () {
        slugInput.val() == "" ? slug = nameInput.val() : slug = slugInput.val();
        defaultSlug.text(convertToSlug(slug.trim()));
    });

    deleteBtn.click(function () {
        var that = $(this);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        }, function () {
            that.parent().submit();
        });
    });
});