function swalError() {
    swal({
        title: "Oops...",
        text: "Something went wrong!",
        confirmButtonColor: "#EF5350",
        type: "error"
    });
}