$(document).ready(function () {
    var nameInput = $('input[name="name"]');
    var slugInput = $('input[name="slug"]');
    var defaultSlug = $('#default-slug');

    nameInput.on('input', function () {
        slugInput.val() == "" ? slug = nameInput.val() : slug = slugInput.val();
        defaultSlug.text(convertToSlug(slug.trim()));
    });

    slugInput.on('input', function () {
        slugInput.val() == "" ? slug = nameInput.val() : slug = slugInput.val();
        defaultSlug.text(convertToSlug(slug.trim()));
    });
});