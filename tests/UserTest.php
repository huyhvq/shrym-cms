<?php

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Contracts\Auth\Authenticatable;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    public function __construct()
    {
        $this->user = [
            'name'                  => 'Huy Huynh',
            'username'              => 'heoquay99',
            'password'              => '123123',
            'password_confirmation' => '123123',
            'email'                 => 'me@huyhuynh.io'
        ];
    }

    public function test_new_user_registrator()
    {
        $this->visit('/register');
        foreach ($this->user as $field => $value) {
            $this->type($value, $field);
        }
        $this->check('acceptTerms')
            ->press('Register');
        $this->see_new_user_in_database();
        $this->is_auth_user();
        $this->seePageIs('/home')->see('Home');
    }

    protected function see_new_user_in_database()
    {
        $this->seeInDatabase('users', $this->get_new_user()->toArray());
    }

    protected function is_auth_user()
    {
        $this->be($this->get_new_user());
    }

    protected function get_new_user()
    {
        return $user = User::where('username', $this->user['username'])->first();
    }
}
