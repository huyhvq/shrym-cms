<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Models\Permission;
use Illuminate\Support\Facades\App;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        if (!App::isDownForMaintenance() && !App::runningInConsole()) {
            foreach ($this->getPermisisons() as $permisison) {
                $gate->define($permisison->name, function ($user) use ($permisison) {
                    return $user->hasRole($permisison->roles);
                });
            }
        };
    }

    public function getPermisisons()
    {
        return Permission::with('roles')->get();
    }
}
