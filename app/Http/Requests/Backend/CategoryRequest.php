<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class CategoryRequest extends Request
{

    protected $rules = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')) {
            $this->rules = [
                'name'      => 'required|min:3|max:255',
                'slug'      => 'unique:categories|max:255',
                'parent_id' => 'required|exists:categories,id',
                'public'    => 'required',
            ];
        } elseif ($this->isMethod('put') || $this->isMethod('patch')) {
            $id = $this->route('categories');
            $this->rules = [
                'parent_id' => sprintf('numeric|exists:categories,id|changeParrentNode:%s', $id)
            ];
        }
        return $this->rules;
    }
}
