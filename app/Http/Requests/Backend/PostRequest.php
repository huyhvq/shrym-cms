<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class PostRequest extends Request
{

    protected $rules = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')) {
            $this->rules = [
                'name'      => 'required|min:3|max:255',
                'slug'      => 'unique:posts|max:255',
                'content'   => 'required',
                'public'    => 'required',
                'category'  => 'required|exists:categories,id',
                'thumbnail' => 'image',
            ];
        } elseif ($this->isMethod('put') || $this->isMethod('patch')) {
            $id = $this->route('posts');
            $this->rules = [
                'name'      => 'required|min:3|max:255',
                'slug'      => 'required|max:255|unique:posts,slug,' . $id,
                'content'   => 'required',
                'public'    => 'required',
                'category'  => 'required|exists:categories,id',
                'thumbnail' => 'image',
            ];
        }
        return $this->rules;
    }
}
