<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\PostRequest as Request;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\MessageBag;
use Exception;
use Carbon\Carbon;
use DB;

class PostController extends Controller
{
    /**
     * @var Guard
     */
    private $auth;
    /**
     * @var MessageBag
     */
    private $messageBag;

    /**
     * @var Upload Image Path
     */
    private $uploadImagePath = 'assets/images/uploads/';

    public function __construct(Guard $auth, MessageBag $messageBag)
    {
        $this->auth = $auth;
        $this->messageBag = $messageBag;
    }

    public function index()
    {

    }

    public function create()
    {
        $categories = Category::getNestedOptions(true);
        return view('backend.limitless.posts.create', compact('categories'));
    }

    public function store(Request $request)
    {
        try {
            //use transaction
            DB::transaction(function () use ($request) {
                $author = $this->auth->user()->getAuthIdentifier();
                $request->has('slug') ?: $slug = str_slug($request->get('name'), '-');
                $request->merge(compact('author', 'slug'));
                $data = $request->except('_token', 'thumbnail');
                //upload file
                if ($request->hasFile('thumbnail')) {
                    $data['thumbnail'] = $this->uploadImages($request->file('thumbnail'), $slug);
                }
                //create new post
                $post = Post::create($data);
                $post->makeIntoCategory($request->category);
            });
            //redirect back if success
            $this->messageBag->add('swalSuccess', 'You has created a new post.');
            return redirect()->back()->withErrors($this->messageBag);
        } catch (Exception $e) {
            app()->abort(500, $e->getMessage());
        }
    }

    public function edit($id)
    {
        try {
            $post = Post::findOrFail($id);
            $post->category = $post->categories->first()->id;
            $categories = Category::getNestedOptions(true);
            return view('backend.limitless.posts.edit', compact('categories', 'post'));
        } catch (Exception $e) {
            app()->abort(404, sprintf('Can not not find the post with ID: %s', $id));
        }
    }

    public function update(Request $request, $id)
    {
        try {
            //use transaction
            DB::transaction(function () use ($request, $id) {
                $data = $request->except('_token', '_method', 'thumbnail', 'category');
                //upload image if any
                if ($request->hasFile('thumbnail')) {
                    $data['thumbnail'] = $this->uploadImages($request->file('thumbnail'), $data['slug']);
                }
                //find & update post
                $post = Post::findOrFail($id);
                $post->update($data);
                $post->makeIntoCategory($request->category);
            });
            //redirect with success
            $this->messageBag->add('swalSuccess', 'You has updated post.');
            return redirect()->back()->withErrors($this->messageBag);
        } catch (Exception $e) {
            app()->abort(500, $e->getMessage());
        }
    }

    protected function uploadImages($image, $imageName = null)
    {
        !is_null($imageName) ?: $imageName = rand(111111, 999999);
        $uploadFileName = sprintf('%s-%s.%s', $imageName, Carbon::now()->timestamp, $image->getClientOriginalExtension());
        $image->move(public_path($this->uploadImagePath), $uploadFileName);
        return $this->uploadImagePath . $uploadFileName;
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
//        return Datatables::of(User::query())->make(true);
    }
}
