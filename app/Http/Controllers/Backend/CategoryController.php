<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CategoryRequest as Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;

use App\Models\Category;
use App\Models\Repositories\CategoryRepository;


class CategoryController extends Controller
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var MessageBag
     */
    private $messageBag;

    public function __construct(CategoryRepository $categoryRepository, MessageBag $messageBag)
    {
        $this->categoryRepository = $categoryRepository;
        $this->messageBag = $messageBag;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryRepository->buildOption();
        $tableCategoriesData = $this->categoryRepository->buildTableTree();
        return view('backend.limitless.category.index', compact('categories', 'tableCategoriesData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->has('slug') ? '' : $request->merge(['slug' => str_slug($request->get('name'), '-')]);
        $this->categoryRepository->createCate($request->except('_token'));
        $this->messageBag->add('swalSuccess', 'You has created new category.');
        return redirect()->back()->withErrors($this->messageBag);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);
        if ($category->isRoot()) {
            return redirect()->back();
        }
        $categories = $this->categoryRepository->buildOption($id);
        return view('backend.limitless.category.edit', compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->categoryRepository->updateCate($request->except(['_token', '_method']), $id);
        $this->messageBag->add('swalSuccess', 'You has created new category.');
        return redirect()->back()->withErrors($this->messageBag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($this->categoryRepository->buildOption());
        $cat = Category::find($id);
        if (!$cat->canDelete()) {
            return redirect()->back()->withErrors($cat->getMessages());
        }
    }

}
