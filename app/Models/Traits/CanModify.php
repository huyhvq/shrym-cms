<?php

namespace App\Models\Traits;

use Illuminate\Support\MessageBag;


trait CanModify
{
    protected $message;

    public function __construct()
    {
        $this->message = new MessageBag();
    }

    public function canDelete()
    {
        if ($this->isRoot()) {
            $this->message->add('swalError',trans('backend.category.cantDeleteRootNode'));
        } elseif (!$this->isLeaf()) {
            $this->message->add('sfError',trans('backend.category.nodeHasChild'));
        }
        return !$this->isRoot() && $this->isLeaf();
    }

    public function canMove($id, $parent)
    {
//        $category = Category::findOrFail($id);
//        $parent = Category::findOrFail($parent);
//        if ($parent->insideSubtree($category)) {
//            $this->message = trans('backend.category.cantMoveYourSelf');
//            return false;
//        }
//        return true;
    }

    public function getMessages()
    {
        return $this->message;
    }
}