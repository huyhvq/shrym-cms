<?php namespace App\Models\Traits;

trait CategoryQuery
{
    public static function getNestedOptions($exceptRoot = false)
    {
        $categories = self::getNestedList('name', null, '—');
        if ($exceptRoot) {
            array_forget($categories, 1);
        }
        return $categories;
    }
}