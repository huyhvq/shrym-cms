<?php

namespace App\Models\Repositories;

use App\Models\Category as CategoryModel;
use Bosnadev\Repositories\Eloquent\Repository;

class CategoryRepository extends Repository
{
    public function model()
    {
        return CategoryModel::class;
    }

    public function buildOption()
    {
        return $this->model->getNestedList('name', null, '—');
    }

    public function buildTableTree()
    {
        $categories = $this->model->where('name', '=', 'Root')->first()->getDescendants();
        return $categories;
    }

    public function createCate(array $category)
    {
        $parrent = CategoryModel::find($category['parent_id']);
        $newCategory = CategoryModel::create($category);
        $newCategory->makeChildOf($parrent);
    }

    public function updateCate(array $data, $id, $attribute = "id")
    {
        $this->update(array_except($data, ['parent_id']), $id, $attribute);
        $cate = CategoryModel::find($id);
        if ($cate->parent_id != $data['parent_id']) {
            $cate->makeChildOf(CategoryModel::find($data['parent_id']));
        }
    }
}