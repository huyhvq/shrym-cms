<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function givePermissionTo($permission)
    {
        if (is_string($permission)) {
            return $this->permissions()->save(
                Permission::whereName($role)->firstOrFail()
            );
        }

        $this->permissions()->save($permission);
    }

    public function givePermissionsTo($permissions)
    {
        return $this->permissions()->sync($permissions);
    }
}