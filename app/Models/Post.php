<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'thumbnail', 'description', 'content', 'public', 'author'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function makeIntoCategory($categories)
    {
        if (!is_array($categories)) {
            $categories = compact('categories');
        }
        $this->categories()->sync($categories);
    }
}
