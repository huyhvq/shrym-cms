<?php namespace App\Validators;

use App\Models\Category as CategoryModel;

class Category
{
    public function changeParrentNode($attribute, $value, $parameters, $validator)
    {
        $editCategory = CategoryModel::find($parameters[0]);
        $parentCategory = CategoryModel::find($value);

        if ($parentCategory->insideSubtree($editCategory)) {
            return false;
        }
        return true;
    }
}